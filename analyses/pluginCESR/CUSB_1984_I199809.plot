BEGIN PLOT /CUSB_1984_I199809/d01-x01-y01
Title=$\pi^+\pi^-$ mass in $\Upsilon(2S)\to\Upsilon(1S)\pi^+\pi^-$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{MeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [MeV]
LogY=0
END PLOT
