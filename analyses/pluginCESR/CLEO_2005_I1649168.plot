BEGIN PLOT /CLEO_2005_I1649168/d01-x01-y07
Title=Scaled momentum spectrum of $D_s^\pm$ mesons in $\Upsilon(4S)$ decays
XLabel=x
YLabel=$\text{d}B/\text{d}x$ [$\%$]
END PLOT
BEGIN PLOT /CLEO_2005_I1649168/d01-x01-y08
Title=Scaled momentum spectrum of $D_s^\pm$ mesons in $\Upsilon(5S)$ decays
XLabel=x
YLabel=$\text{d}B/\text{d}x$ [$\%$]
END PLOT