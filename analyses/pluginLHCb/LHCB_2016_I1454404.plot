# BEGIN PLOT /LHCB_2016_I1454404/*
XTwosidedTicks=1
YTwosidedTicks=1
LegendAlign=r
LegendXPos=0.95
LeftMargin=1.8
YLabelSep=7.5
LogY=0
ErrorBars=1
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d01-x01-y01
Title=Cross-section for $W^+j$ Production
XLabel=$\sqrt{s}$
YLabel=$\sigma(W^{+}j)$ [pb]
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d01-x01-y02
Title=Cross-section for $W^{-}j$
XLabel=$\sqrt{s}$
YLabel=$\sigma(W^{-}j)$ [pb]
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d01-x01-y03
Title=Cross-section for $Zj$
XLabel=$\sqrt{s}$
YLabel=$\sigma(Zj)$ [pb]
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d02-x01-y01
Title=Ratio of $W^\pmj$ to $Zj$ production
XLabel=$\sqrt{s}$
YLabel=$R_{WZ}$
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d02-x01-y02
Title=Ratio of $W^+j$ to $Zj$ production
XLabel=$\sqrt{s}$
YLabel=$R_{W^+Z}$
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d02-x01-y03
Title=Ratio of $W^-j$ to $Zj$ production
XLabel=$\sqrt{s}$
YLabel=$R_{W^-Z}$
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d02-x01-y04
Title=Ratio of $W^+j$ to $W^-j$ production
XLabel=$\sqrt{s}$
YLabel=$R_{W^\pm}$
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d03-x01-y01
Title=Asymmetry of $W^+j$ and $W^-j$ production
XLabel=$\sqrt{s}$
YLabel=$A_{W^\pm}$
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d04-x01-y01
Title=Cross-section for $W^+j$ production v $\eta^{\mu}$
XLabel=$\eta^{\mu}$
YLabel=$\sigma(W^{+}j)$ [pb]
# + any additional plot settings you might like, see make-plots documentation
#END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d04-x01-y02
Title=Cross-section for $W^-j$ production v $\eta^{\mu}$
XLabel=$\eta^{\mu}$
YLabel=$\sigma(W^{-}j)$ [pb]
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d05-x01-y01
Title=Cross-section for $W^+j$ production v $\eta^{\rm jet}$
XLabel=$\eta^{\rm jet}$
YLabel=$\sigma(W^+j)$ [pb]
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d05-x01-y02
Title=Cross-section for $W^-j$ production v $\eta^{\rm jet}_T$
XLabel=$\eta^{\rm jet}$
YLabel=$\sigma(W^-j)$ [pb]
# + any additional plot settings you might like, see make-plots documentation
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d06-x01-y01
Title=Cross-section for $W^+j$ production v $p_{\rm T}^{\rm jet}$
XLabel=$p^{\rm jet}_{\rm T}$ [GeV]
YLabel=$\sigma(W^+j)$ [pb]
# + any additional plot settings you might like, see make-plots documentation
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d06-x01-y02
Title=Cross-section for $W^-j$ production v $p_{\rm T}^{\rm jet}$
XLabel=$p^{\rm jet}_{\rm T}$ [GeV]
YLabel=$\sigma(W^-j)$ [pb]
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d07-x01-y01
Title=Cross-section for $Z_j$ production v $y^{\rm Z}$
XLabel=$y^{\rm Z}$
YLabel=$\sigma(Zj)$ [pb]
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d08-x01-y01
Title=Cross-section for $Zj$ production v $\eta^{\rm jet}$
XLabel=$\eta^{\rm jet}$
YLabel=$\sigma(Zj)$ [pb]
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d09-x01-y01
Title=Cross-section for $Zj$ production v $p^{\rm jet}_{\rm T}$
XLabel=$p^{\rm jet}_{\rm T}$ [GeV]
YLabel=$\sigma(Zj)$ [pb]
# END PLOT

# BEGIN PLOT /LHCB_2016_I1454404/d10-x01-y01
Title=Cross-section for $Z_j$ production v $\Delta\phi(Z,{\rm jet})$
XLabel=$\Delta\phi(Z,{\rm jet})$ [rad]
YLabel=$\sigma(Zj)$ [pb]
LogY=1
# END PLOT
