# BEGIN PLOT /CMS_2018_I1620050/d01-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton
XLabel=$p_\text{T}(\text{lepton})\;[\text{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\text{T}(\text{lepton})}\;[\text{GeV}^{-1}]$
LogY=1
YMin=3e-5
YMax=0.1
RatioPlotYMin=0.5
RatioPlotYMax=1.5
RatioPlotYSize=2.6
PlotSize=10.7,7.2
#END PLOT

# BEGIN PLOT /CMS_2018_I1620050/d02-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton
XLabel=$p_\text{T}(\text{jet})\;[\text{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\text{T}(\text{jet})}\;[\text{GeV}^{-1}]$
YMin=3e-5
YMax=0.1
PlotSize=10.7,7.2
#END PLOT

# BEGIN PLOT /CMS_2018_I1620050/d03-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton
XLabel=$p_\text{T}(\text{t}) [\text{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\text{T}(\text{t})}\;[\text{GeV}^{-1}]$
YMin=4e-5
YMax=0.05
PlotSize=10.7,7.2
#END PLOT

# BEGIN PLOT /CMS_2018_I1620050/d04-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton
XLabel=$y(\text{t})$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dy(\text{t})}$
LogY=0
YMin=0
YMax=0.7
PlotSize=10.7,7.2
#END PLOT

# BEGIN PLOT /CMS_2018_I1620050/d05-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton
XLabel=$p_\text{T}(\text{t}\bar{\text{t}})\;[\text{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\text{T}(\text{t}\bar{\text{t}})}\;[\text{GeV}^{-1}]$
YMin=3e-5
YMax=0.2
PlotSize=10.7,7.2
#END PLOT

# BEGIN PLOT /CMS_2018_I1620050/d06-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton
XLabel=$y(\text{t}\bar{\text{t}})$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dy(\text{t}\bar{\text{t}})}$
LogY=0
YMin=0
YMax=0.9
PlotSize=10.7,7.2
#END PLOT

# BEGIN PLOT /CMS_2018_I1620050/d07-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton
XLabel=$M(\text{t}\bar{\text{t}})\;[\text{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dM(\text{t}\bar{\text{t}})}\;[\text{GeV}^{-1}]$
LogY=1
YMin=3e-5
YMax=0.01
PlotSize=10.7,7.2
#END PLOT

# BEGIN PLOT /CMS_2018_I1620050/d08-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton
XLabel=$\Delta\phi(\text{t},\bar{\text{t}})\;[\text{Radian}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{d\,\Delta\phi(\text{t},\bar{\text{t}})}\;[\text{Radian}^{-1}]$
YMin=1e-2
YMax=10
PlotSize=10.7,7.2
#END PLOT


