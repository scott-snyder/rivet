BEGIN PLOT /CMS_2019_I1764472/d01-x01-y01
Title=CMS, 13 TeV, jet mass in boosted top quark decays
XLabel=$m_\text{jet}$
YLabel=$\frac{d\sigma}{dm_\text{jet}} \frac{\text{fb}}{\text{GeV}}$
LogY=0
END PLOT


BEGIN PLOT /CMS_2019_I1764472/d02-x01-y01
Title=CMS, 13 TeV, normalized jet mass in boosted top quark decays
XLabel=$m_\text{jet}$
YLabel=$\frac{1}{\sigma} \frac{d\sigma}{dm_\text{jet}} \frac{1}{\text{GeV}}$
LogY=0
END PLOT


# ... add more histograms as you need them ...
